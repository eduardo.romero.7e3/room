package cat.itb.room;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import cat.itb.room.database.AppDatabase;
import cat.itb.room.database.QuestionDao;
import cat.itb.room.database.Question;
import cat.itb.room.database.QuestionRepository;

public class QuestionActivity extends AppCompatActivity {
    static AppDatabase db;
    static QuestionDao dao;
    static QuestionRepository repo;

    private TextView questionText;
    private TextView questionNumber;
    private TextView timeRemaining;

    private List<Question> questions;
    EditText answerEditText;
    private CountDownTimer timer;
    private String name;

    private int count = 0, score = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_activity);

        db = AppDatabase.getInstance(this.getApplicationContext());
        dao = db.questionDao();
        repo = new QuestionRepository(dao);

        questionText = findViewById(R.id.questionText);
        questionNumber = findViewById(R.id.questionNumberText);
        timeRemaining = findViewById(R.id.secondsRemainingText);
        answerEditText = findViewById(R.id.answerEditText);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            name = bundle.getString("name");
        }


        if (repo.getAllQuestions().isEmpty()) {
            questions = new ArrayList<Question>();
            questions.add(new Question("8 x 5", "40"));
            questions.add(new Question("10 - 13", "-3"));
            questions.add(new Question("10000 / 100", "100"));
            questions.add(new Question("20 / 5", "4"));
            questions.add(new Question("9 ^(3)", "729"));
            questions.add(new Question("-1 ^(17)", "-1"));
            questions.add(new Question("180 / 9", "20"));
            questions.add(new Question("718 - 35", "683"));
            questions.add(new Question("1 ^(-1)", "1"));
            questions.add(new Question("7 x 12", "84"));
            questions.add(new Question("90 / 5", "18"));
            questions.add(new Question("sqrt(81)", "9"));
            questions.add(new Question("13 - (-26)", "39"));
            questions.add(new Question("-8 x 20", "-160"));
            questions.add(new Question("4 x (-8 + 3)", "-20"));
            questions.add(new Question("5 ^(4)", "625"));
            questions.add(new Question("(5 x 6) + [10 x (-3)]", "0"));
            questions.add(new Question("10 ^(8)", "100000000"));
            questions.add(new Question("sqrt(sqrt(81))", "3"));
            questions.add(new Question("[-20 ^(2)] + (-22)", "378"));

            for (Question q : questions) {
                repo.insertQuestion(q);
            }
        }

        questions = repo.getQuestions();

        answerEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    checkAnswer(answerEditText.getText().toString());
                    handled = true;
                }
                return handled;
            }
        });

        showQuestion();
    }

    public void showQuestion() {
        questionText.setText(questions.get(count).getQuestion());
        questionNumber.setText("Question " + (count + 1) + " of 5");
        answerEditText.setText("");
        timer = new CountDownTimer(10000, 1000) {
            public void onTick(long millisUntilFinished) {
                timeRemaining.setText("Seconds remaining: " + (millisUntilFinished / 1000 + 1));
            }

            public void onFinish() {
                timeRemaining.setText("Time's up!");
                checkAnswer(answerEditText.getText().toString());
            }
        }.start();
    }

    public void checkAnswer(String answer) {
        timer.cancel();
        answerEditText.setEnabled(false);
        if (questions.get(count).getAnswer().matches(answer)) {
            score += 1;
            Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_SHORT).show();
        } else Toast.makeText(getApplicationContext(), "Wrong!", Toast.LENGTH_SHORT).show();

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                answerEditText.setEnabled(true);
                if (count < questions.size() - 1) {
                    count++;
                    showQuestion();
                } else {
                    Intent intent = new Intent(QuestionActivity.this, FinalScoreActivity.class);
                    intent.putExtra("name", name);
                    intent.putExtra("score", score);
                    startActivity(intent);
                }
            }
        }.start();
    }
}
