package cat.itb.room;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.room.database.AppDatabase;
import cat.itb.room.database.Score;
import cat.itb.room.database.ScoreDao;
import cat.itb.room.database.ScoreRepository;

public class ScoresActivity extends AppCompatActivity {

    static AppDatabase db;
    static ScoreDao dao;
    static ScoreRepository repo;

    private MyAdapter adapter;
    private RecyclerView recyclerView;
    private Button deleteAllButton;
    private List<Score> scores;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_activity);

        db = AppDatabase.getInstance(this.getApplicationContext());
        dao = db.scoreDao();
        repo = new ScoreRepository(dao);
        deleteAllButton = findViewById(R.id.deleteAllButton);

        scores = repo.getAllScores();
        recyclerView = findViewById(R.id.scoreRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyAdapter(scores);
        recyclerView.setAdapter(adapter);
        adapter.setScores(scores);

        deleteAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scores.clear();
                repo.deleteAllScores();
                adapter.setScores(scores);
            }
        });

    }
}
