package cat.itb.room;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EnterNameActivity extends AppCompatActivity {

    private EditText nameEditText;
    private Button beginButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.name_activity);

        nameEditText = findViewById(R.id.nameEditText);
        beginButton = findViewById(R.id.beginButton);

        beginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextActivity();
            }
        });

        nameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    nextActivity();
                    handled = true;
                }
                return handled;
            }
        });
    }

    public void nextActivity() {
        if (nameEditText.getText().toString().matches("")) {
            Toast.makeText(getApplicationContext(), "You must enter a name!", Toast.LENGTH_SHORT)
                    .show();
        } else {
            Intent intent = new Intent(EnterNameActivity.this, QuestionActivity.class);
            intent.putExtra("name", nameEditText.getText().toString());
            startActivity(intent);
        }
    }
}
