package cat.itb.room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.room.database.Score;

public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {
    private List<Score> list;

    public MyAdapter(List<Score> list) {
        this.list = list;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setScores(List<Score> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}

class MyViewHolder extends RecyclerView.ViewHolder {
    TextView nameText, scoreText;


    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        nameText = itemView.findViewById(R.id.nameText);
        scoreText = itemView.findViewById(R.id.scoreText);
    }

    public void bindData(Score score) {
        nameText.setText(score.getUserName());
        scoreText.setText(String.valueOf(score.getScore()));
    }
}
