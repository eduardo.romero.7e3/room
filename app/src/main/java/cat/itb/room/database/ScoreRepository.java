package cat.itb.room.database;

import java.util.List;

public class ScoreRepository {
    ScoreDao dao;

    public ScoreRepository(ScoreDao dao) {
        this.dao = dao;
    }

    public List<Score> getAllScores() {
        return dao.getAllScores();
    }

    public void insertScore(Score s) {
        dao.insertScore(s);
    }

    public void deleteAllScores() {
        dao.deleteAllScores();
    }
}
