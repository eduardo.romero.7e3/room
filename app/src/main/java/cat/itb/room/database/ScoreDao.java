package cat.itb.room.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ScoreDao {
    @Query("SELECT * FROM score_table ORDER BY score DESC")
    List<Score> getAllScores();

    @Insert
    void insertScore(Score s);

    @Query("DELETE FROM score_table")
    void deleteAllScores();
}
