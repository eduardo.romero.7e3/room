package cat.itb.room.database;

import java.util.List;

public class QuestionRepository {
    QuestionDao dao;

    public QuestionRepository(QuestionDao dao) {
        this.dao = dao;
    }

    public List<Question> getQuestions() {
        return dao.getQuestions();
    }

    public List<Question> getAllQuestions() {
        return dao.getAllQuestions();
    }

    public void insertQuestion(Question q) {
        dao.insertQuestion(q);
    }

    public void deleteAllQuestions() {
        dao.deleteAllQuestions();
    }
}
