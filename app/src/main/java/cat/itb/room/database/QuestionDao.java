package cat.itb.room.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface QuestionDao {
    @Query("SELECT * FROM question_table ORDER BY RANDOM() LIMIT 5")
    List<Question> getQuestions();

    @Query("SELECT * FROM question_table")
    List<Question> getAllQuestions();

    @Insert
    void insertQuestion(Question q);

    @Query("DELETE FROM question_table")
    void deleteAllQuestions();
}
