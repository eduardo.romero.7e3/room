package cat.itb.room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cat.itb.room.database.AppDatabase;
import cat.itb.room.database.Score;
import cat.itb.room.database.ScoreDao;
import cat.itb.room.database.ScoreRepository;

public class FinalScoreActivity extends AppCompatActivity {

    static AppDatabase db;
    static ScoreDao dao;
    static ScoreRepository repo;

    private TextView finalScoreText;
    private Button returnButton;
    private String name;
    private int score;
    private Score s;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_score_activity);

        db = AppDatabase.getInstance(this.getApplicationContext());
        dao = db.scoreDao();
        repo = new ScoreRepository(dao);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            name = bundle.getString("name");
            score = bundle.getInt("score");
        }

        finalScoreText = findViewById(R.id.finalScoreText);
        returnButton = findViewById(R.id.returnButton);

        s = new Score(name, score);
        repo.insertScore(s);

        finalScoreText.setText("Congratulations " + name + "! Your final score is: " + score);

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FinalScoreActivity.this, WelcomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
